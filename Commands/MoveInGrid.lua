function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "points",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local position = {}

	position = parameter.points

	for i = 1, #position do
   	   CMD.MOVE(position[i])
    end

    return RUNNING

end
