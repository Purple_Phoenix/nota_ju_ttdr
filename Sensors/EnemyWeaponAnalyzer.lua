local EVAL_PERIOD_DEFAULT = 0 -- this sensor is not caching any values, it evaluates itself on every request

function getInfo()
	return 
	{
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function (enemies)
	
	local enemyUnitsInfo = {}
	local enemiesCount = #enemies

	for i=1, enemiesCount do
		local enemyUnitID = enemies[i]
        local enemyUnitDefID = Spring.GetUnitDefId( enemyUnitID )
        local firstWeaponDefID = UnitDefs[ enemyUnitDefID ].weapons[1].weaponDef
        enemyUnitsInfo[#enemyUnitsInfo+1] = 
                  {
                       id =  enemyUnitID,
                       defID =  enemyUnitDefID,
                       unitName = UnitDefs[ enemyUnitDefID ].name,
                       weaponName = WeaponDefs[ firstWeaponDefID ].name,
                       weaponRange = WeaponDefs[ firstWeaponDefID ].range
}  
	end
		
	return enemyUnitsInfo

end