local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(pos, radius)
local team = Spring.GetLocalTeamID()
local ally_units = Spring.GetUnitsInCylinder (pos.x, pos.z, radius, team)

	return ally_units
end